using System;
using System.Linq;

namespace AdventOfCode.Days
{
    public class Day5 : AOCInput
    {
        public override string Day => nameof(Day5);

        public override void Solve()
        {
            var seats = Input.Select(i =>
            {
                var checkDigit = CalculatePosition(i);
                var row = checkDigit >> 3;
                var column = (checkDigit << 3 ^ checkDigit);
                return new {
                    Row = row,
                    CheckDigit = checkDigit,
                    Column = column
                };
            });

            Console.WriteLine($"Highest seat ID:\t{seats.Max(s => s.CheckDigit)}");

            var seatIds = seats
                .Select(s => s.CheckDigit)
                .OrderBy(k => k)
                .ToArray();

            var mySeat = seatIds
                .Where((s, i) => i != 0 && i != seatIds.Length && !seatIds.Contains(s - 1))
                .FirstOrDefault() - 1;
            Console.WriteLine($"My seat ID:\t{mySeat}");
        }

        public int CalculatePosition(string input)
        {
            int id = 0;

            foreach (char s in input) 
            {
                id = id << 1;
                if (s == 'B' || s == 'R')
                    id++;
            }

            return id;
        }
    }
}