using System;
using System.Linq;

namespace AdventOfCode.Days
{
    public class Day6 : AOCInput
    {
        public override string Day => nameof(Day6);

        public override Action OrganiseInput => () => {
            Input = System.IO.File.ReadAllText(InputPath)
                .Split("\r\n\r\n");
        };
        public override void Solve()
        {
            var uniqueAnswers = 
                Input.Select(i => i
                    .Where(c => !new [] {'\r', '\n'}.Contains(c))
                    .Distinct()
                    .Count()
                );

            var groupedAnswers = (
                from g in Input.Select(i => i.Split("\r\n"))
                let allGroupAnswers = g.SelectMany(s => s.ToArray())
                select allGroupAnswers.Where(a => g.All(s => s.Contains(a)))
                    .Distinct()
                    .Count()
            );

            Console.WriteLine($"Total unique 'yes' answers:\t{uniqueAnswers.Sum()}");
            Console.WriteLine($"Total groups with 'yes answers:\t{groupedAnswers.Sum()}");
        }
    }
}