using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;

namespace AdventOfCode.Days
{
    public class Day4 : AOCInput
    {
        public override string Day => nameof(Day4);

        private Dictionary<string, Func<string, bool>> _validators = new Dictionary<string, Func<string, bool>>
        {
            { "byr", (val) => int.TryParse(val, out var y) && y >= 1920 && y <= 2002 },
            { "iyr", (val) => int.TryParse(val, out var y) && y >= 2010 && y <= 2020 },
            { "eyr", (val) => int.TryParse(val, out var y) && y >= 2020 && y <= 2030 },
            { "hgt", (val) => !string.IsNullOrEmpty(val) && (
                                (val.EndsWith("cm") && int.TryParse(Regex.Replace(val, "[^0-9]", ""), out var hcm) && hcm >= 150 && hcm <= 193) ||
                                (val.EndsWith("in") && int.TryParse(Regex.Replace(val, "[^0-9]", ""), out var hin) && hin >= 59 && hin <= 76)
                              )},
            { "hcl", (val) => Regex.IsMatch(val ?? "", "#[a-fA-F0-9]{6}") },
            { "ecl", (val) => new [] {"amb", "blu", "brn", "gry", "grn", "hzl", "oth"}.Contains(val) },
            { "pid", (val) => val?.Length == 9 },
            { "cid", _ => true}
        };

        public override Action OrganiseInput => () => {
            Input = System.IO.File.ReadAllText(InputPath)
                .Split("\r\n\r\n");
        };

        public override void Solve()
        {
            string parseAttribute(string key, string[] value) {
                return value.SingleOrDefault(s => s.StartsWith(key))?.Split(':')[1];
            }

            var documents = Input
                .Select(i => i
                .Replace("\r\n", " ")
                .Split(' '))
                .Select(i => new Dictionary<string,string> {
                    { "byr", parseAttribute("byr", i)},
                    { "iyr", parseAttribute("iyr", i)},
                    { "eyr", parseAttribute("eyr", i)},
                    { "hgt", parseAttribute("hgt", i)},
                    { "hcl", parseAttribute("hcl", i)},
                    { "ecl", parseAttribute("ecl", i)},
                    { "pid", parseAttribute("pid", i)},
                    { "cid", parseAttribute("cid", i)}
                });

            var validDocuments = documents
                .Where(d => _validators.Keys.SkipLast(1).All(r => d[r] != null));

            var moreValidPassports = documents // seriously, this again?
                .Where(d => d.All(k => _validators[k.Key](k.Value)));

            Console.WriteLine($"Valid Passports:\t{validDocuments.Count()}");
            Console.WriteLine($"Even valid-er passports:\t{moreValidPassports.Count()}");
        }
    }
}