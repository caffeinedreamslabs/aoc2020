using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Days
{
    public class Day3 : AOCInput
    {
        public override string Day => nameof(Day3);

        private bool IsTree(char c) => c == '#';

        private List<long> OMGTREES = new List<long>();

        public override void Solve()
        {
            Console.WriteLine("Scenario A:");
            Traverse(1,1);
            Console.WriteLine("Scenario B:");
            Traverse(3,1);
            Console.WriteLine("Scenario C:");
            Traverse(5,1);
            Console.WriteLine("Scenario D:");
            Traverse(7,1);
            Console.WriteLine("Scenario E:");
            Traverse(1,2);

            Console.WriteLine($"Product of trees = {OMGTREES.Aggregate((x, y) => x*y)}");
        }

        private void Traverse(int longVelocity, int latVelocity)
        {
            var longitude = 0;
            var treeCounter = 0;

            for (var latitude = 0; latitude < Input.Count; latitude += latVelocity)
            {
                var currentTerrain = Input[latitude];
                
                longitude = longitude + longVelocity >= currentTerrain.Length
                    ? longitude = (longitude + longVelocity) - currentTerrain.Length
                    : longitude + longVelocity;

                var newTerrain = Input.ElementAtOrDefault(latitude + latVelocity);

                if (newTerrain != null && IsTree(newTerrain[longitude]))
                    treeCounter++;
            }
            OMGTREES.Add(treeCounter);
            Console.WriteLine($"Encountered {treeCounter} trees");
        }
    }
}