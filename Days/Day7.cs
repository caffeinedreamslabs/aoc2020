using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using BagMap = System.Collections.Generic.Dictionary<string, System.Collections.Generic.IEnumerable<(string colour, int count)>>;

namespace AdventOfCode.Days
{
    public class Day7 : AOCInput
    {
        public override string Day => nameof(Day7);

        public override void Solve()
        {
            var pattern = @"((?'outerbag'.*\w*\s)bags\s)contain\s(?'contains'no other bags\.|(?'contents'\d.*)(bag|bags))";

            var map = Input
                .Select(i => {
                    var matches = Regex.Matches(i, pattern);
                    return new {
                        containerColour = matches[0].Groups["outerbag"].Value.TrimEnd(),
                        contents = ProcessColours(matches[0].Groups["contents"].Value)
                    };
                }).ToDictionary(x => 
                    x.containerColour, 
                    x => x.contents.Distinct()
                        .Select(c => (c, x.contents.Count(i => c == i))));

            var mapToShinyGold = map.Keys.Count(b => CanContain(map, b, "shiny gold"));
            Console.WriteLine($"shiny gold bags can do one {mapToShinyGold} times");

            Console.WriteLine($"shiny gold bags must contain {BagsContained(map, "shiny gold")}");
        }

        private bool CanContain(BagMap map, string containingColour, string containedColour) =>
            map[containingColour].Any(b => b.colour == containedColour || CanContain(map, b.colour, containedColour));
        
        private int BagsContained(BagMap map, string containingColor) =>
            map[containingColor].Sum(b => b.count + b.count * BagsContained(map, b.colour));

        private string[] ProcessColours(string contents)
        {
            var pattern = @"((?'description'(?'quantity'\d)\s(?'colour'.*?))($|bag|bags))";
            var matches = Regex.Matches(contents, pattern);
            
            return matches.SelectMany(m => {
                if (int.TryParse(m.Groups["quantity"].Value, out var q))
                    return Enumerable
                        .Repeat(m.Groups["colour"].Value.TrimEnd(), q)
                        .ToArray();
                return Array.Empty<string>();
            }).ToArray();
        }
    }
}