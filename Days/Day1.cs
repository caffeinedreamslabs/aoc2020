using System;
using AdventOfCode;
using System.Linq;

namespace AdventOfCode.Days
{
    public class Day1 : AOCInput
    {
        public override string Day => nameof(Day1);

        public override void Solve()
        {
            SolveFor2();
            SolveFor3();
        }

        private void SolveFor3()
        {
            var inputs = Input
                .Select(i => int.Parse(i))
                .ToList();

            (int i1, int i2, int i3) = (0,0,0);
            
            for (int w = 0; w < inputs.Count; w++)
            {
                i1 = inputs[w];

                for (int i = 0; i < inputs.Count; i ++)
                {
                    i2 = inputs[i];

                    i3 = inputs.FirstOrDefault(n => 2020 - n - i2 - i1 == 0);
                    if (i3 != 0)
                        break;
                }
                if (i1 * i2 * i3 != 0)
                    break;
            }

            Console.WriteLine($"For 3:\t{i1*i2*i3} ({i1}, {i2}, {i3})");
        }

        private void SolveFor2()
        {
            int i1 = 0;
            int i2 = 0;
            
            var input = Input
                .Select(i => int.Parse(i))
                .ToList();

            foreach (var i in input)
            {
                i1 = input.Where(n => i + n == 2020).FirstOrDefault();
                if (i1 != 0)
                {
                    i2 = i;
                    break;
                }
            }

            Console.WriteLine($"For 2:\t{i2 * i1} ({i2}, {i1})");
        }
    }
}
