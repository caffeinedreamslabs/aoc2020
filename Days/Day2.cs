using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Days
{
    public class Day2 : AOCInput
    {
        public override string Day => nameof(Day2);

        public override void Solve()
        {
            var validEntries = GetValidEntries(false)
                .ToArray();

            var reallyValidEntries = GetValidEntries(true)
                .ToArray();

            Console.WriteLine($"Valid password count:\t{validEntries.Length}");
            Console.WriteLine($"Really valid password count:\t{reallyValidEntries.Length}");
        }

        private IEnumerable<string> GetValidEntries(bool useUpdatedPolicy)
        {
            foreach (var pword in Input)
            {
                var parts = GetParts(pword);
                if (useUpdatedPolicy)
                {
                    if(IsReallyValid(parts.pw, parts.policy))
                        yield return parts.pw;
                }
                else if (IsValid(parts.pw, parts.policy))
                    yield return parts.pw;
            }
        }

        private bool IsValid(string pw, string policy)
        {
            var pwChars = pw.ToArray();
            var pwStats = new Dictionary<char, int>();
            foreach (var c in pwChars)
            {
                if (!pwStats.ContainsKey(c))
                    pwStats.Add(c, pwChars.Count(p => p == c));
            }

            var range = ParseRules(policy);

            return pwStats.Any(p => 
                p.Key == range.Predicate
                && p.Value <= range.Pos2
                && p.Value >= range.Pos1);
        }

        private bool IsReallyValid(string pw, string policy)
        {
            var range = ParseRules(policy);

            return new [] {pw[range.Pos1 -1], pw[range.Pos2 -1]}.Count(s => s == range.Predicate) == 1;
        }

        private PolicyRules ParseRules(string policy)
        {
            var pattern = @"(\d+)-(\d+)\s(.)";
            var policyRules = Regex.Matches(policy, pattern)[0];
            return new PolicyRules  {
                Pos1 = int.Parse(policyRules.Groups[1].Value),
                Pos2 = int.Parse(policyRules.Groups[2].Value),
                Predicate = policyRules.Groups[3].Value.ToCharArray()[0]
            };
        }

        private (string policy, string pw) GetParts (string pword)
        {
            var parts = pword.Split(':');

            return (parts[0], parts[1].Trim());
        }

    }

    internal class PolicyRules
    {
        public int Pos1 { get; set; }

        public int Pos2 { get; set; }

        public char Predicate { get; set; }
    }
}