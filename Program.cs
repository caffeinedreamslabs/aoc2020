﻿using System;

namespace AdventOfCode
{
    class Program
    {
        static void Main(string[] args)
        {
            var days = new Action[]
            {
                () => new Days.Day1().Solve(),
                () => new Days.Day2().Solve(),
                () => new Days.Day3().Solve(),
                () => new Days.Day4().Solve(),
                () => new Days.Day5().Solve(),
                () => new Days.Day6().Solve(),
                () => new Days.Day7().Solve()
            };

            foreach (var day in days)
            {
                day();
                Console.WriteLine("=================== Merry Christmas =====================");
            }
        }
    }
}
