using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode
{
    public abstract class AOCInput : IAOCInput
    {
        public abstract string Day { get; }

        public IList<string> Input { get; protected set; }

        public abstract void Solve();

        public AOCInput()
        {
            Console.WriteLine($"{Day.ToUpper()}\r\n----------");

            OrganiseInput();
        }

        public string InputPath => $"Inputs\\{Day}.txt";

        public virtual Action OrganiseInput => () => {
            Input = System.IO.File.ReadAllLines(InputPath).ToList();
        };
    }
}