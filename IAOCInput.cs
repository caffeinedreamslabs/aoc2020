using System.Collections.Generic;

namespace AdventOfCode
{
    public interface IAOCInput
    {
        string Day { get; }

        IList<string> Input { get; }
    }
}
